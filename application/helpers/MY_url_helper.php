<?php

    /**
     * Проверка корректности идентификатора в url
     * @param string $id - Id записи
     * @return void
     */
    function is_valid_url_id($id)
    {
        // Если не число, значит ошибка
        if (!is_numeric($id))
            show_404();
    }

    /**
     * Проверка корректности идентификатора в url
     * @param string $param
     * @return void
     */
    function is_empty_id($param)
    {
        // Если не число, значит ошибка
        if (empty($param))
            show_404();
    }