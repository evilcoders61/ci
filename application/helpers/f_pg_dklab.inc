<?php
/**
* (C) 2005 Dmitry Koterov, http://forum.dklab.ru/users/DmitryKoterov/
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* See http://www.gnu.org/copyleft/lesser.html
*/


function hstore_update($hstore, $arr=array()){
  $a = array_merge((array)hstore2arr($hstore), (array)$arr);
  return arr2hstore($a);
}


function arr2hstore($value) {
  static $ESCAPE = '"\\';

  if (is_null($value)) {
      return null;
  }
  if (!is_array($value)) {
  }
  $parts = array();
  foreach ($value as $key => $value) {
      $parts[] = 
          '"' . addcslashes($key, $ESCAPE) . '"' .
          '=>' .
          ($value === null? "NULL" : '"' . addcslashes($value, $ESCAPE) . '"');
  }
  return join(", ", $parts);
}

function hstore2arr($str, $start=0){
  static $p;
  if ($start==0) $p=0;
  $result = array();
  $c = _charAfterSpaces($str, $p);
  if ($c === false) {
    return array();
  }
  
  while (1) {
      $c = _charAfterSpaces($str, $p);
      
      // End of string.
      if ($c === false) {
          break;
      }
      
      // Next element.
      if ($c == ',') {
          $p++;
          continue;
      }
      
      // Key.
      $key = _readString($str, $p);
      
      // '=>' sequence.
      _charAfterSpaces($str, $p);
      if (substr($str, $p, 2) != '=>') {}
      $p += 2;
      _charAfterSpaces($str, $p);
      
      // Value.
      $value = _readString($str, $p);
      if (!strcasecmp($value, "null")) {
          $result[$key] = null;
      } else {
          $result[$key] = $value;
      }
  }
  return $result;

}


function arr2pgarr($value){
  foreach ((array)$value as $inner) {
    if (is_array($inner)) $parts[] = arr2pgarr($inner);
    elseif ($inner === null) {
      $parts[] = 'NULL';
    } else {
      $parts[] = '"' . addcslashes($inner, "\"\\") . '"'; 
    }
  }
  return '{' . join(",", (array)$parts) . '}';
}


function pgarr2arr($str, $start=0){
  static $p;
  if ($start==0) $p=0;
  $result = array();
  
  // Leading "{".
  $c = _charAfterSpaces($str, $p);
  if ($c != '{') {
    return;
  }
  $p++;
  
  // Array may contain:
  // - "-quoted strings
  // - unquoted strings (before first "," or "}")
  // - sub-arrays
  while (1) {
      $c = _charAfterSpaces($str, $p);
      
      // End of array.
      if ($c == '}') {
          $p++;
          break;
      }
      
      // Next element.
      if ($c == ',') {
          $p++;
          continue;
      }
      
      // Sub-array.
      if ($c == '{') {
          $result[] = pgarr2arr($str, $p);
          continue;
      }
      
      // Unquoted string.
      if ($c != '"') {
      	$len = strcspn($str, ",}", $p);
      	$v = stripcslashes(substr($str, $p, $len));
      	if (!strcasecmp($v, "null")) {
      	    $result[] = null;
      	} else {
              $result[] = $v;
      	}
      	$p += $len;
          continue;
      }
      
      // Quoted string.
      $m = null;
      if (preg_match('/" ((?' . '>[^"\\\\]+|\\\\.)*) "/Asx', $str, $m, 0, $p)) {
          $result[] = stripcslashes($m[1]);
          $p += strlen($m[0]);
          continue;
      }
  }
  
  return $result;

}


function _charAfterSpaces($str, &$p){
  $p += strspn($str, " \t\r\n", $p);
  return substr($str, $p, 1);
}

function _readString($str, &$p){
  $c = substr($str, $p, 1);
      
  // Unquoted string.
  if ($c != '"') {
      $len = strcspn($str, " \r\n\t,=>", $p);
      $value = substr($str, $p, $len);
      $p += $len;
      return stripcslashes($value);
  }
      
  // Quoted string.
  $m = null;
  if (preg_match('/" ((?' . '>[^"\\\\]+|\\\\.)*) "/Asx', $str, $m, 0, $p)) {
      $value = stripcslashes($m[1]);
      $p += strlen($m[0]);
      return $value;
  }
}


define('DBSKIP', log(0));

function p(){
  return $GLOBALS["Placeholder"]->_expandPlaceholders(func_get_args(), false);
}

class Placeholders{

    var $_identPrefix = '';

    public function __construct() {
        $args = func_get_args();
        $total = false;
        return $this->_expandPlaceholders($args, false);
    }

    function escape($s, $isIdent=false){
      if (!$isIdent)
          return "'" . pg_escape_string((string)$s) /*str_replace("'", "''", $s)*/ . "'";
      else
          return '"' . str_replace('"', '_', $s) . '"';
    }

    function setIdentPrefix($prx){
        $old = $this->_identPrefix;
        if ($prx !== null) $this->_identPrefix = $prx;
        return $old;
    }

    function _expandPlaceholders(&$queryAndArgs, $useNative=false) {

        if (!is_array($queryAndArgs)) {
            $queryAndArgs = array($queryAndArgs);
        }

        $this->_placeholderNativeArgs = $useNative? array() : null;
        $this->_placeholderArgs = array_reverse($queryAndArgs);

        $query = array_pop($this->_placeholderArgs); // array_pop is faster than array_shift

        // Do all the work.
        $query = $this->_expandPlaceholdersFlow($query);

        if ($useNative) {
            array_unshift($this->_placeholderNativeArgs, $query);
            $queryAndArgs = $this->_placeholderNativeArgs;
        } else {
            $queryAndArgs = array($query);
        }
        return $query;
    }

    function _expandPlaceholdersFlow($query) {
        $re = '{
            (?>
                # Ignored chunks.
                (?>
                    # Comment.
                    -- [^\r\n]*
                )
                  |
                (?>
                    # DB-specifics.
                    "   (?> [^"\\\\]+|\\\\"|\\\\)*    "   |
                    \'  (?> [^\'\\\\]+|\\\\\'|\\\\)* \'   |
                    /\* .*?                          \*/      # comments
                )
            )
              |
            (?>
                # Optional blocks
                \{
                    ( (?> (?>[^{}]*)  |  (?R) )* )             #1
                \}
            )
              |
            (?>
                # Placeholder
                (\?) ( [_dsafn\#]? )                           #2 #3
            )
        }sx';
        $query = preg_replace_callback(
            $re,
            array(&$this, '_expandPlaceholdersCallback'),
            $query
        );
        return $query;
    }


    /**
     * string _expandPlaceholdersCallback(list $m)
     * Internal function to replace placeholders (see preg_replace_callback).
     */
    function _expandPlaceholdersCallback($m)
    {
        // Placeholder.
        if (!empty($m[2])) {
            $type = $m[3];

            // Idenifier prefix.
            if ($type == '_') {
                return $this->_identPrefix;
            }

            // Value-based placeholder.
            if (!$this->_placeholderArgs) return 'DBSIMPLE_ERROR_NO_VALUE';
            $value = array_pop($this->_placeholderArgs);

            // Skip this value?
            if ($value === DBSKIP) {
                $this->_placeholderNoValueFound = true;
                return '';
            }

            // First process guaranteed non-native placeholders.
            switch ($type) {
                case 'a':
                    if (!$value) $this->_placeholderNoValueFound = true;
                    if (!is_array($value)) return 'DBSIMPLE_ERROR_VALUE_NOT_ARRAY';
                    $parts = array();
                    foreach ($value as $k=>$v) {
                        $v = $v === null? 'NULL' : $this->escape($v);
                        if (!is_int($k)) {
                            $k = $this->escape($k, true);
                            $parts[] = "$k=$v";
                        } else {
                            $parts[] = $v;
                        }
                    }
                    return join(", ", $parts);
                case "#":
                    // Identifier.
                    return $this->escape($value, true);
                    //
                    // TODO: add array support for is_array($value).
                    //
                case 'n':
                    // NULL-based placeholder.
                    return empty($value)? 'NULL' : intval($value);
            }

            // Native arguments are not processed.
            if ($this->_placeholderNativeArgs !== null) {
                $this->_placeholderNativeArgs[] = $value;
                return '?';
            }

            // In non-native mode arguments are quoted.
            if ($value === null) return 'NULL';
            switch ($type) {
                case '':
                    if (!is_scalar($value)) return "DBSIMPLE_ERROR_VALUE_NOT_SCALAR";
                    return $this->escape($value);
                case 'd':
                    return intval($value);
                case 'f':
                    return str_replace(',', '.', floatval($value));
                case 's':
                    return pg_escape_string($value);
            }
            // By default - escape as string.
            return $this->escape($value);
        }

        // Optional block.
        if (isset($m[1]) && strlen($block=$m[1])) {
            $prev = @$this->_placeholderNoValueFound;
            $block = $this->_expandPlaceholdersFlow($block);
            $block = $this->_placeholderNoValueFound? '' : ' ' . $block . ' ';
            $this->_placeholderNoValueFound = $prev; // recurrent-safe
            return $block;
        }

        // Default: skipped part of the string.
        return $m[0];
    }

}

$Placeholder = new Placeholders();
?>