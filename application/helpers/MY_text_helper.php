<?php

    include "f_pg_dklab.inc";

	function strim($pStr)
	// Вырезаем все пробелы из строки
	// Параметры: $pStr - строка
	{
	   while ( strpos($pStr,' ') !== false )
	   {
		   $pStr = str_replace(' ','', $pStr);
	   };
	   
	   return $pStr;
	}
	
	
	function get_db_value($pInput, $pDb)
	// Отображение параметров
	// Возвращает значение
	{
		$strCode = "if (\$this->validation->\$pInput != '') 
		{
			
		}
		else {
			return \$pDb;
		}
		";
		
		eval($strCode); 
	}

	function str_boolean ($pValue)
	{
		if ($pValue == 1) {
			return "есть";
		}
		else
		{
			return "нет";
		}
		
	}
	
	function short_message ( $pStr, $pStrLimiter = null, $pWordLimiter = null )
	// Превью текстового сообщения: max - 4 слова, не более 200 символов
	// Параметры: $pStr - строка
	{ 
	   if ($pStrLimiter != null) {
	      $str = mb_substr($pStr, 0, $pStrLimiter, 'UTF-8');
	      
	      if (strlen($pStr) > $pStrLimiter)
	      	 $str = $str . "..."; 
	      	 
	      return $str;
	   }
	      
	   if ($pWordLimiter != null)
	   	  return word_limiter($pStr, $pWordLimiter);
	}
	
	function short_topic ( $pStr )
	// Превью топика: max - 14 слов, не более 200 символов
	// Параметры: $pStr - строка
	{
	   return word_limiter($pStr, 14);
	}
	
	function reply_subject ( $pStr )
	// Заголовок для ответных сообщений "Re:Заголовок"
	// Параметры: $pStr - строка
	{
	   $pos1 = strpos($pStr, "Re");
	   $pos2 = strpos($pStr, "Re(");
	   
	   if (strlen($pStr) >= 3) {
	   	  $pos3 = strpos($pStr, ")", 3 ); 
	   }
	   else {
	   	  $pos3 = 0; 
	   }
	   
	   if ($pos1 === false)
	   {
	   	   $str	= "Re:" . $pStr;
	   }
	   else 
	   {
	   	  if ($pos2 === false)
	   	  {
	   	  	  $str	= "Re(1):" . $pStr;
	   	  }
	   	  else 
	   	  {
	   	  	  if ($pos3 === false)
	   	  	  {
	   	  	  	 $str	= "Re:" . $pStr;
	   	  	  }
	   	  	  else {
	   	  	  	 $newre = substr($pStr, 3, $pos3 - 3);
	   	  	  	 $newre = $newre + 1;
	   	  	  	 $str = substr($pStr, 0, 3) . $newre . substr($pStr, $pos3);
	   	  	  }
	   	  }
	   }
	   
	   return character_limiter($str, 250);
	}
	
	
	function plural_ru ( $pStr, $pTermsArray, $pCount )
	// Меняет единственное число на множественное. Для русских слов
	// Параметры: $pStr - слово без окончания, $pTermsArray - массив 3-х типов окончаний: 1, 2-4, 5-10, $pCount - количество 
	{	   
	   if ( ( strlen($pCount) >= 2 ) ) {
	   	  
	   	  $pEnd = substr($pCount, (strlen($pCount) - 2 ), 2); 
		  
	   	  if ( ($pEnd >= 14) and ($pEnd <= 19) )
	   	  	 return $pStr . $pTermsArray[2];
	   }
	   
	   $pEnd = substr($pCount, (strlen($pCount) - 1), 1);
	   
	   if ( $pEnd == 1 ) {
	   	  return $pStr . $pTermsArray[0];
	   }
	   elseif ( ( $pEnd ) >= 2 and ( $pEnd <= 4 ) )
	   {
	   	  return $pStr . $pTermsArray[1];
	   }
	   elseif ( ( ( $pEnd >= 5 ) and ( $pEnd <= 9 ) ) or ( $pEnd == 0 ) )
	   {
	   	  return $pStr . $pTermsArray[2];
	   }
	   else {
	   	  return $pStr;
	   }
	   
	}

	function str_price($pStrPrice)
	// Обработка цены (вставка пробела между разрядами)
	{
	   $str = "";
	   
	   if ($pStrPrice == "")
	   	  return "0";
	   
	   if (strlen($pStrPrice) > 3) {
	   	  $str = substr($pStrPrice, 0, strlen($pStrPrice)-3) . ", " . substr($pStrPrice, -3);  	  
	   }
	   else {
	   	  $str = $pStrPrice;
	   }
	   
	   return $str;
	}
	
	// Превращает отношение между двумя числами в проценты
	function str_percent($pCount, $pAllCount)
	{
		if ($pAllCount == 0)
			return 0;
			
		return round(($pCount / $pAllCount) * 100, 2);
	}
	
	function str_gender($pGenderType)
	{
		if ($pGenderType == "m")
			return "М";
		else if ($pGenderType == "f")
			return "Ж";
		else 
			return "";
	}
	
	function str_gender2($pGenderType)
	{
		if ($pGenderType == "m")
			return "Мужчины";
		else if ($pGenderType == "f")
			return "Женщины";
		else 
			return "Без ограничений";
	}
	
	function str_age_range($ageFrom, $ageTo)
	{
		$str = "";
		
		if (trim($ageFrom) == "" && trim($ageTo) == "")
			return "Без ограничений";
		
		if (trim($ageFrom) != "")
			$str = $str . "от " . $ageFrom;
			
		if (trim($ageTo) != "")
			$str = $str . " до " . $ageTo;
			
		$str = $str . " лет";
			
		return $str;
	}

    // Возвращает анименование типа оплаты по id
    function str_pay_type($type)
    {
        $pay_types = array(0 => "Предоплата", 1 => "Карта", 2 => "Наличные");

        if (isset($pay_types[$type]))
            return $pay_types[$type];

        return "";
    }

    // Преобразование временного массива в строковый интервал
    function str_prepare_time_range($val)
    {
        $timearray = pgarr2arr($val);

        if (count($timearray) == 2)
            return $timearray[0] . " — " . $timearray[1];
        else if (count($timearray) == 1)
            return $timearray[0];
        else
            return "";
    }

    function str_action_state($is_blocked)
    {
        if ($is_blocked == "f")
            return "Активный";
        else if ($is_blocked == "t")
            return "Заблокирован";
    }

    function css_action_state($is_blocked)
    {
        if ($is_blocked == "f")
            return "status-1";
        else if ($is_blocked == "t")
            return "status-2";
    }

    function css_action_state2($is_active)
    {
        if ($is_active == "t")
            return "status-1";
        else if ($is_active == "f")
            return "status-2";
    }
	
	// Возвращает противоположный статус акции
	function str_action_reverse_state($pState)
	{
		if ($pState == "active")
			return "приостановить";
		else if ($pState == "stopped")
			return "возобновить";
		else "";
	}
	
	function str_pre($pStr)
	// Аналог тега <PRE>
	{
		return str_replace("\r\n", "<br>", $pStr);
	}
	
	// Возвращает selected
	function str_option_status($type, $value)
	{
		if ($type == $value)
			return "selected";
			
		return "";
	}
	
	function str_option_status2($type, $value, $result_value, $else_value = "")
	{
		if ($type == $value)
			return $result_value;
		else return $else_value;
	}
	
	// 
	function str_prepare_date($date)
	{
		return str_replace("-", " / ", $date);
	}
	
	// Проверяет наличие http:// и https:// в ссылке и добавляет, если нужно
	function str_prepare_link($url)
	{
		if ((strpos($url, "http://") === false) && strpos($url, "https://") === false)
			return "http://" . $url;
		else 
			return $url;
	}
	
	// Проверяем адрес картинки (если на локальном сервере, дополняем путь)
	function str_prepare_logo($logo, $userId)
	{
		if ((strpos($logo, "http://") === false) && strpos($logo, "https://") === false)
			return base_url() . "uploads/" . $userId . "/" . $logo;
		else 
			return $logo;
	}
	
	function getval($obj, $prop, $defaultValue = NULL)
	{
		if (isset($obj) && $prop == "")
            if ($defaultValue != NULL && $obj == "")
                return $defaultValue;
            else
			    return htmlspecialchars($obj);

		if (isset($obj) && isset($obj->$prop))
			return htmlspecialchars($obj->$prop);

		return $defaultValue;
	}

    function get_arr_val($array, $key, $defaultValue = NULL)
    {
        if (!is_array($array))
            return $defaultValue;

        if (array_key_exists($key, $array)) {
            return $array[$key];
        }

        return $defaultValue;
    }

    /**
     * Конвертирует массив в строку через запятую
     * @param json $val
     * @return void
     */
    function pgarr2str($val)
    {
        $str = "";

        $arr = pgarr2arr(val);

        foreach($arr as $row)
        {
            $str = $str != "" ? $str . ", " . $row : $row;
        }
    }


    /**
     * Конвертирует запись лога об изменении полей из JSON в нормальный текст
     * @param json $val
     * @return void
     */
    function get_change_data_from_json($template, $object, $objects, $event_types)
    {
        $change_array = json_decode($object->change_data);

        $change_str = "<ul class='history_list'>";

        foreach ($change_array as $key => $value) {
            $change_str = $change_str . "<li><span class='field-name'>$key</span>: ". $value->old ." &#8594; " . $value->new . "
                 | </li>";
        }

        $change_str = $change_str . "</ul>";


        $str = str_replace('{object_name}', get_arr_val($objects, $object->table_name, ""), $template);
        $str = str_replace('{object_table}', $object->table_name, $str);
        $str = str_replace('{event_type}', get_arr_val($event_types, $object->event_type, ""), $str);
        $str = str_replace('{change_data}', $change_str, $str);

        return $str;
    }
    /*
    function get_change_data_from_json($template, $json)
    {
        $change_array = json_decode($json);

        $str = "<ul class='history_list'>";

        foreach ($change_array as $key => $value) {
            $str = $str . "<li><span class='field-name'>$key</span>: ". $value->old ." --> " . $value->new . "
            </li>";
        }

        $str = $str . "</ul>";

        return $str;
    }
    */

?>