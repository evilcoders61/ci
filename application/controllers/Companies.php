<?php
class Companies extends BackEnd
{

    public function __construct()
    {
        $this->ObjectId = 1;

        parent::__construct();

        $this->load->helper('url');

        $this->load->helper(array('form', 'language', 'date', 'text', 'rights', 'array'));
        $this->load->library(array('form_validation'));
        $this->load->model( 'Company_m','', TRUE );
    }

    function index()
    {
        $data = array();
        $this->_basic_func($data, 1);

        $this->viewData["MainBloc"] = $this->load->view('inner/blocks/company', $data, true);

        if (isset($_REQUEST['ajax'])) {
            echo $this->viewData["MainBloc"];
        } else {
            $this->viewData["HeaderBloc"] = $this->load->view('inner/blocks/header', $data, true);
            $this->viewData["NavigationBloc"] = $this->load->view('inner/blocks/navigation', $data, true);
            $this->viewData["FooterBloc"] = $this->load->view('inner/blocks/footer', $data, true);
            $this->load->view('inner/pages/interface_inner', $this->viewData);
        }
    }

    /**
     * Форма добавления нового пользователя
     * @return void
     */
    function add()
    {
        $data = array();
        $this->_basic_func($data, 2);

        $data["Company"] = null;

        $this->viewData["MainBloc"] = $this->load->view('inner/blocks/company_add', $data, true);

        if (isset($_REQUEST['ajax'])) {
            echo $this->viewData["MainBloc"];
        } else {
            $this->viewData["HeaderBloc"] = $this->load->view('inner/blocks/header', $data, true);
            $this->viewData["NavigationBloc"] = $this->load->view('inner/blocks/navigation', $data, true);
            $this->viewData["FooterBloc"] = $this->load->view('inner/blocks/footer', $data, true);
            $this->load->view('inner/pages/interface_inner', $this->viewData);
        }
    }

    /**
     * Форма редактирования контактного лица
     * @return void
     */
    function edit($id = null)
    {
        $data = array();
        $this->_basic_func($data, 3);

        $data["Company"] = $this->Company_m->GetCompanyById($id);
        $data["AccountList"] = $this->Company_m->GetAccountList($id);

        $this->viewData["MainBloc"] = $this->load->view('inner/blocks/company_add', $data, true);

        if (isset($_REQUEST['ajax'])) {
            echo $this->viewData["MainBloc"];
        } else {
            $this->viewData["HeaderBloc"] = $this->load->view('inner/blocks/header', $data, true);
            $this->viewData["NavigationBloc"] = $this->load->view('inner/blocks/navigation', $data, true);
            $this->viewData["FooterBloc"] = $this->load->view('inner/blocks/footer', $data, true);
            $this->load->view('inner/pages/interface_inner', $this->viewData);
        }
    }


    /**
     * AJAX: Получение найденных компаний
     * @return void
     */
    function ajax_get_company_list()
    {
        $this->_basic_func();

        $searchParam = "";
        $searchStr = (isset($_POST["search_str"])) ? $_POST["search_str"] : null;
        $searchType = (isset($_POST["search_param"])) ? $_POST["search_param"] : null;

        switch ($searchType) {
            case "":
                $searchParam = "c.name ilike {param} or a.number ilike {param} or c.legal ilike {param}";
                break;
            case 0:
                $searchParam = "c.name ilike {param}";
                break;
            case 1:
                $searchParam = "a.number ilike {param}";
                break;
        }

        $dataCompanyList = $this->Company_m->GetCompanyList($searchParam, $searchStr, $this->CompanyIdsStr);

        $data["CompanyList"] = $dataCompanyList;
        $data["CompanyCount"] = count($dataCompanyList);

        $result["company_list"] = $this->load->view('inner/blocks/search_company_list.php', $data, true);
        $result["company_count"] = count($dataCompanyList);

        echo json_encode($result);
    }

    /**
     * AJAX: Получение найденных компаний для выбора
     * @return void
     */
    function ajax_get_company_select_list()
    {
        $this->_basic_func();

        $searchParam = "";
        $searchStr = (isset($_POST["search_str"])) ? $_POST["search_str"] : null;
        $searchType = (isset($_POST["search_param"])) ? $_POST["search_param"] : null;

        switch ($searchType) {
            case "":
                $searchParam = "c.name ilike {param} or a.number ilike {param} or c.legal ilike {param}";
                break;
            case 0:
                $searchParam = "c.name ilike {param}";
                break;
            case 1:
                $searchParam = "a.number ilike {param}";
                break;
        }

        $dataCompanyList = $this->Company_m->GetCompanyList($searchParam, $searchStr, $this->CompanyIdsStr);

        $data["CompanyList"] = $dataCompanyList;
        $data["CompanyCount"] = count($dataCompanyList);

        $result["company_list"] = $this->load->view('inner/blocks/select_company_list.php', $data, true);
        $result["company_count"] = count($dataCompanyList);

        echo json_encode($result);
    }

    /**
     * AJAX: Получение найденных личных счетов для выбора
     * @return void
     */
    function ajax_get_account_select_list()
    {
        $this->_basic_func();

        $searchParam = "";
        $searchStr = (isset($_POST["search_str"])) ? $_POST["search_str"] : null;

        $dataAccountList = $this->Company_m->GetAccountListByStr($searchStr, $this->CompanyIdsStr);

        $data["CompanyList"] = $dataAccountList;
        $data["CompanyCount"] = count($dataAccountList);

        $result["company_list"] = $this->load->view('inner/blocks/select_account_list.php', $data, true);
        $result["company_count"] = count($dataAccountList);

        echo json_encode($result);
    }


    /**
     * AJAX: Добавление нового контактного лица
     * @return void
     */
    function ajax_add_company()
    {
        $this->_basic_func();

        $roles = array();

        $result["error"] = "";

        $company_id = "";
        $name = (isset($_POST["name"])) ? $_POST["name"] : null;
        $legal = (isset($_POST["legal"])) ? $_POST["legal"] : null;
        $accounts = (isset($_POST["accounts"])) ? json_decode($_POST["accounts"]) : array();

        if (trim($name) != "")
        {
            $company_id = $this->Company_m->AddCompany($name, $legal);

            foreach ($accounts as $account)
                if (trim($account->value) != "")
                    $this->Company_m->AddAccount($company_id, $account->value);
        }
        else
            $result["error"] = "Не заполнены обязательные поля";

        $result["companyId"] = $company_id;
        $result["is_new"] = "";

        echo json_encode($result);
    }

    /**
     * AJAX: Изменение информации о компании
     * @return void
     */
    function ajax_update_company()
    {
        $this->_basic_func();

        $roles = array();

        $result["error"] = "";

        $name = (isset($_POST["name"])) ? $_POST["name"] : null;
        $company_id = (isset($_POST["companyid"])) ? $_POST["companyid"] : null;
        $legal = (isset($_POST["legal"])) ? $_POST["legal"] : null;
        $accounts = (isset($_POST["accounts"])) ? json_decode($_POST["accounts"]) : array();

        if (trim($name) != "" && trim($company_id) != "")
        {
            $this->Company_m->UpdateCompany($company_id, $name, $legal);

            foreach ($accounts as $account) {
                if (trim($account->value) != "")
                    if (trim($account->id != ""))
                        $this->Company_m->UpdateAccount($company_id, $account->id, $account->value);
                    else
                        $this->Company_m->AddAccount($company_id, $account->value);
            }
        }
        else
            $result["error"] = "Не заполнены обязательные поля";

        $result["companyId"] = $company_id;
        $result["is_new"] = "";

        echo json_encode($result);
    }

    /**
     * AJAX: Удаление информации о компании
     * @return void
     */
    function ajax_delete_company()
    {
        $this->db->trans_start();

        $company_id = (isset($_POST["id"])) ? $_POST["id"] : null;
        $this->Company_m->DeleteCompany($company_id);

        $this->Company_m->DeleteCompany($company_id);

        $this->db->trans_complete();
    }

    // Проверка уникальности номера лицевого счета
    function ajax_check_account_number()
    {
        if (array_key_exists('number', $_POST) && array_key_exists('id', $_POST)) {
            if ( $this->Company_m->CheckAccountNumber($_POST["number"], $_POST["id"]) > 0  ) {
                echo json_encode(FALSE);
            } else {
                echo json_encode(TRUE);
            }
        }

    }
}
