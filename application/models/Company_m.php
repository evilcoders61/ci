<?php

class Company_m extends MY_Model {

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Получение списка компаний
     * @param string $paramValue значение
     * @return array - список лиц
     */
    function GetCompanyList($searchParam, $searchStr, $pCompanyIdStr = "")
    {
        $strSQL = "SELECT c.*,
                          CASE
		                    WHEN changedate IS NULL THEN createdate
                          END AS last_change_date,
                          string_agg(a.number, ', ') AS accounts
					 FROM company AS c
					 LEFT JOIN account AS a
                       ON a.company_id = c.id
					WHERE 1=1 AND (" . str_replace("{param}", $this->db->escape("%".$searchStr."%"), $searchParam) . ")" ;

        // Фильтруем по компаниям
        if (!empty($pCompanyIdStr))
            $strSQL = $strSQL . " AND c.id IN ($pCompanyIdStr)";

        $strSQL = $strSQL . " GROUP BY c.id, c.name";

        $query = $this->db->query( $strSQL );

        if ($query)
            return $query->result();

        return NULL;
    }

    /**
     * Получение списка счетов компании
     * @param int $companyId Id компании
     * @return array - список лиц
     */
    function GetAccountList($companyId, $pCompanyIdStr = "")
    {
        $strSQL = "SELECT a.*
					 FROM account AS a
					WHERE company_id = ?";

        // Фильтруем по компаниям
        if (!empty($pCompanyIdStr))
            $strSQL = $strSQL . " AND a.company_id IN ($pCompanyIdStr)";

        $query = $this->db->query( $strSQL, array($companyId) );

        if ($query)
            return $query->result();

        return NULL;
    }

    /**
     * Получение списка счетов компании
     * @param int $companyId Id компании
     * @return array - список лиц
     */
    function GetAccountListByStr($paramValue, $pCompanyIdStr = "")
    {
        $strSQL = "SELECT a.*,
                          c.name
					 FROM account AS a
                    INNER JOIN company AS c
                       ON a.company_id = c.id
					WHERE (a.number ilike " . $this->db->escape("%".$paramValue."%") . "
                       OR c.name ilike " . $this->db->escape("%".$paramValue."%") . ")";

        // Фильтруем по компаниям
        if (!empty($pCompanyIdStr))
            $strSQL = $strSQL . " AND a.company_id IN ($pCompanyIdStr)";

        $query = $this->db->query( $strSQL );

        if ($query)
            return $query->result();

        return NULL;
    }

    /**
     * Получение информации о компании по Id
     * @param int $id Id компании
     * @return object - компания
     */
    function GetCompanyById($id)
    {
        $strSQL = "select c.*
					 from company as c
					where id = ?";

        $query = $this->db->query( $strSQL, array($id) );

        foreach ($query->result() as $row) {
            return $row;
        }

        return null;
    }

    /**
     * Получение информации о лицевом счете
     * @param string $number номер счета
     * @return array - список лиц
     */
    function GetAccountByNumber($number)
    {
        $strSQL = "select a.*
					 from account as a
					where a.number = ?";

        $query = $this->db->query( $strSQL, array($number) );

        foreach ($query->result() as $row) {
            return $row;
        }

        return null;
    }

    /**
     * Добавление информации о компании
     * @param string $name название
     * @param string $legal юридическая информация
     * @return void
     */
    function AddCompany($name, $legal)
    {
        $strSQL = "INSERT INTO company (name, legal) VALUES(?, ?)";

        $this->db->query($strSQL, array($name, $legal));

        return $this->db->insert_id();
    }

    /**
     * Добавление информации о лицевом счете
     * @param int $company_id Id компании
     * @param string $account номер лицевого счета
     * @return int Id новой записи
     */
    function AddAccount($company_id, $account)
    {
        $strSQL = "INSERT INTO account (number, company_id) VALUES(?, ?)";

        $this->db->query($strSQL, array($account, $company_id));

        return $this->db->insert_id();
    }

    /**
     * Изменение информации о лицевом счете
     * @param int $company_id Id компании
     * @param int $account_id Id лицевого счета
     * @param string $account номер лицевого счета
     * @return void
     */
    function UpdateAccount($company_id, $account_id, $number)
    {
        $strSQL = "UPDATE account
                      SET number = ?
                    WHERE id = ?
                      AND company_id = ?";

        $this->db->query($strSQL, array($number, $account_id, $company_id));
    }

    /**
     * Изменение информации о компании
     * @param int $companyid Id компании
     * @param string $name название
     * @param string $legal юридическая информация
     * @return void
     */
    function UpdateCompany($company_id, $name, $legal)
    {
        $strSQL = "UPDATE company SET name = ?, legal = ? WHERE id = ? RETURNING *";

        $this->db->query($strSQL, array($name, $legal, $company_id));
    }

    /**
     * Удаление информации о компании
     * @param int $company_id Id компании
     * @return void
     */
    function DeleteCompany($company_id)
    {
        $this->db->trans_start();

        $strSQL = "UPDATE person
                      SET account_id = NULL
                    WHERE account_id IN (SELECT id
                                           FROM account
                                          WHERE company_id = ?)";
        $this->db->query($strSQL, array($company_id));

        $strSQL = "DELETE FROM account WHERE company_id = ?";
        $this->db->query($strSQL, array($company_id));

        $strSQL = "DELETE FROM company WHERE id = ?";
        $this->db->query($strSQL, array($company_id));

        $this->db->trans_complete();

    }

    /**
     * Получение кол-ва совпадающих лицевых счетов
     * @param string $number номер
     * @param int $account_id Id счета
     * @return int - кол-во счетов
     */
    function CheckAccountNumber($number, $account_id)
    {
        $strSQL = "SELECT count(*) AS num
                         FROM account
                        WHERE number = ?";

        if (trim($account_id) != "")
            $strSQL = $strSQL . " AND id <> " . $this->db->escape($account_id);

        $query = $this->db->query( $strSQL, array($number));

        foreach ($query->result() as $row) {
            return $row->num;
        }
    }
}

?>